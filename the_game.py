from random import randint


class Unit:
    def __init__(self, name, p_attack=1, p_def=0, heals=100, crit_range=1):
        if all([
            isinstance(heals, int),
            isinstance(p_attack, int),
            isinstance(p_def, int),
            isinstance(name, str),
            isinstance(crit_range, int),
        ]):
            self._name = name
            self._p_attack = p_attack if p_attack > 0 else 1
            self._p_def = p_def if p_def >= 0 else 0
            self._heals = heals if heals > 0 else 100
            self._crit_range = crit_range if crit_range > 1 else 1

        else:
            raise TypeError

    bonus_p_attack = 0
    bonus_p_def = 0
    bonus_heals = 0
    bonus_crit = 0

    def to_equip(self, hat=None, body_armor=None, two_hend=None, left_hend=None, right_hend=None):
        self.bonus_p_attack = 0
        self.bonus_p_def = 0
        self.bonus_heals = 0
        self.bonus_crit = 0
        if isinstance(hat, Hat):
            self.bonus_p_attack += hat._p_attack
            self.bonus_p_def += hat._p_def
            self.bonus_heals += hat._heals
            self.bonus_crit += hat._crit
        if isinstance(body_armor, BodyArmor):
            self.bonus_p_def += body_armor._p_def
            self.bonus_heals += body_armor._heals
        if isinstance(two_hend, TwoHend):
            self.bonus_p_attack += two_hend._p_attack
            self.bonus_crit += two_hend._crit
        else:
            if isinstance(left_hend, OneHend):
                self.bonus_p_attack += left_hend._p_attack
                self.bonus_p_def += left_hend._p_def
                self.bonus_heals += left_hend._heals
                self.bonus_crit += left_hend._crit
            if isinstance(right_hend, OneHend):
                self.bonus_p_attack += right_hend._p_attack
                self.bonus_p_def += right_hend._p_def
                self.bonus_heals += right_hend._heals
                self.bonus_crit += right_hend._crit

        return self

    def damage(self, attack, crit, bonus_p_attack, bonus_crit):
        attack = attack + bonus_p_attack
        crit = crit + bonus_crit
        min_attack = int(attack / 2)
        damage = randint(min_attack, attack)
        chance = randint(1, 100)
        if chance in range(crit):
            print('крит!')
            damage = damage * 2
        return damage

    def attack(self, attacker, defender, land):
        if all([
            isinstance(attacker, Unit),
            isinstance(defender, Unit),
            isinstance(land, Landscape)]
        ):

            loss = 0
            damage = attacker.damage(attacker._p_attack, attacker._crit_range, attacker.bonus_p_attack,
                                     attacker.bonus_crit) + land._bonus_p_attack
            defend_p_def = defender._p_def + defender.bonus_p_def + land._bonus_p_def
            if defend_p_def < damage:
                loss = damage - defend_p_def
                if defender.bonus_heals > 0:
                    defender.bonus_heals = defender.bonus_heals - loss
                    if defender.bonus_heals < 0:
                        defender._heals = defender._heals - defender.bonus_heals
                else:
                    defender._heals = defender._heals - loss
            print(f'{attacker._name} нанес {loss} урона')
            if defender._heals <= 0:
                print(f'{attacker._name} нанес раны не совместимые с жизнью {defender._name}')
        else:
            raise TypeError

    def __str__(self):
        return str(self.__dict__)


class Fight:
    def __init__(self, unit1, unit2):
        if all([
            isinstance(unit1, Unit),
            isinstance(unit2, Unit),
        ]):
            self._unit1 = unit1
            self._unit2 = unit2
        else:
            raise TypeError

    def pvp(self, land):
        while self._unit1._heals > 0 and self._unit2._heals > 0:
            self._unit1.attack(self._unit1, self._unit2, land)
            if self._unit2._heals > 0:
                self._unit2.attack(self._unit2, self._unit1, land)


class Equipment:
    def __str__(self):
        return str(self.__dict__)


class Hat(Equipment):
    def __init__(self, p_attack=0, p_def=0, heals=0, crit=0):
        if all([
            isinstance(p_attack, int),
            isinstance(p_def, int),
            isinstance(heals, int),
            isinstance(crit, int),
        ]):
            self._p_attack = p_attack
            self._p_def = p_def
            self._heals = heals
            self._crit = crit


class BodyArmor(Equipment):
    def __init__(self, p_def=0, heals=0):
        if all([
            isinstance(p_def, int),
            isinstance(heals, int),
        ]):
            self._p_def = p_def
            self._heals = heals


class TwoHend(Equipment):
    def __init__(self, p_attack=0, crit=0):
        if all([
            isinstance(p_attack, int),
            isinstance(crit, int),
        ]):
            self._p_attack = p_attack
            self._crit = crit


class OneHend(Equipment):
    def __init__(self, p_attack=0, p_def=0, heals=0, crit=0):
        if all([
            isinstance(p_attack, int),
            isinstance(p_def, int),
            isinstance(heals, int),
            isinstance(crit, int),
        ]):
            self._p_attack = p_attack
            self._p_def = p_def
            self._heals = heals
            self._crit = crit


class Landscape:
    def __init__(self, bonus_p_attack=0, bonus_p_def=0):
        if all([
            isinstance(bonus_p_attack, int),
            isinstance(bonus_p_def, int),
        ]):
            self._bonus_p_attack = bonus_p_attack
            self._bonus_p_def = bonus_p_def
        else:
            raise TypeError


cuirass = BodyArmor(p_def=10)
helmet = Hat(p_def=5, heals=5)
sword = OneHend(p_attack=15, crit=10)
shield = OneHend(p_def=10, p_attack=-5, crit=-5)

two_handed_sword = TwoHend(p_attack=25, crit=25)
leather_armor = BodyArmor(p_def=5, heals=10)
hat_with_feather = Hat(crit=-5, p_def=-5, heals=20)

Rand_Al_Thor = Unit('Rand_Al_Thor', 30, 5, 100, 1)
Samael = Unit('Samael', 15, 20)

print(Rand_Al_Thor)
print(Samael)
Rand_Al_Thor.to_equip(body_armor=cuirass, hat=helmet, right_hend=sword, left_hend=shield)
Samael.to_equip(two_hend=two_handed_sword, body_armor=leather_armor, hat=hat_with_feather)
Rand_Al_Thor.to_equip(body_armor=cuirass, hat=helmet, right_hend=sword, left_hend=shield)
Samael.to_equip(two_hend=two_handed_sword, body_armor=leather_armor, hat=hat_with_feather)

hills = Landscape(2, 10)

town = Landscape(12, 15)
print(Rand_Al_Thor)
print(Samael)

fight1 = Fight(Rand_Al_Thor, Samael)
fight1.pvp(hills)
fight1.pvp(town)

print(Rand_Al_Thor)
print(Samael)
